import { Request, Response } from 'express';
import { CompanyService } from "../../services/CompanyService";
import { Company } from '../../services/Models/Company';
import { CompanyRest } from '../models/Company/Company';
import { FilterDTO } from '../models/Filter';

export default class CompanyController extends CompanyService
{
    private _service: CompanyService = new CompanyService();

    GetMyAsync = async (req: Request, res: Response, next: any) =>
    {
        const userId = res.locals.user.id;

        const myCompanies = await this._service.GetByUser(userId);
        if(!myCompanies)
            return res.status(404).send("Company not found.");

        return res.status(200).send(myCompanies);
    }

    GetCommentsAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const id = req.params.id;

        const company = await Company.findOne({where: {Id: id}});
        if(company === null)
            return res.status(404).send({message: "Company not found."});

        const companyComments = await this._service.GetCompanyCommentsAsync(id);
        
        return res.status(200).send(companyComments);
        
    }
    
    GetRateAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const companyId = req.params.id

        const company = await this._service.GetCompany(companyId);
        if(company === null)
            return res.status(404).send({message: "Company not found."});
        
        const companyRate = await this._service.GetAvgCompanyRateAsync(companyId);
        if(companyRate === null || companyRate === undefined)
           return res.status(404).send({message: "Unrated Company"});

       return res.status(200).send({AvgRate: companyRate});
    }

    PostAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const userId = res.locals.user.id;
        const body: CompanyRest = res.locals.body;
        
        const existingName = await this._service.GetByName(body.Name);
        if(existingName != null)
            return res.status(409).send({message: "Name already taken."});

        const result = await this._service.PostCompanyAsync(userId, body);
        
        if(result.success)
            return res.status(200).json(req.body);

         return res.status(500).send({message: result.errors.join()});
    }

    FindAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const filter: FilterDTO = res.locals.filter;
        const result = await this._service.FindCompanyAsync(filter);

        return res.status(200).send(result);
    }

    GetAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const id = req.params.id;

        const result = await this._service.GetCompany(id);
        if(!result)
            return res.status(404).json({message: "Company not found"});
        return res.status(200).send({result});
    }

    PutAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const id = req.params.id;
        const body: CompanyRest = res.locals.body;
        
        if(id == null || body == null)
        return res.status(400).send({message: "Missing parameters"}); 
        
        const model = await this._service.GetCompany(id);
        if(!model)
            return res.status(404).json({message: "Job not found"});

            
        const result = await this._service.PutCompanyAsync(id, body);
        if(result.success)
            return res.status(200).json(req.body).send();

        return res.status(500).send({message: result.errors.join()});
    }

    DeleteAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const id = req.params.id;
        if(id == null)
            return res.status(400).send({message: "Missing parameters."});
        
        const model = await this._service.GetCompany(id);
        if(!model)
            return res.status(404).json({message: "Job not found"});
        
        const result = await this._service.DeleteCompanyAsync(id);
        if(result.success)
            return res.status(200).send();
        
        return res.status(500).send({message: result.errors.join()});
    }
}