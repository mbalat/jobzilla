
import { Request, Response } from 'express'
import { JobService } from '../../services/JobService';
import { FilterDTO } from '../models/Filter';
import { JobRest } from '../models/Job/JobRest';

export default class JobController extends JobService
{
    private _service: JobService = new JobService();

    
    FindAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const filter: FilterDTO = res.locals.filter;
        const result = await this._service.FindJobAsync(filter);
        return res.status(200).json({
            total: result.count,
            items: result.rows
        });
    }

    GetAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const id = req.params.id;

        const result = await this._service.GetJobById(id);
        if(result === null)
            return res.status(404).json({message: "Job not found"});

        return res.status(200).json(result);
    }

    PostAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const userId = res.locals.user.id;
        const body: JobRest = res.locals.body;

        const company = await this._service.GetCompanyById(body.CompanyId); 
        if(company?.UserId !== userId)
            return res.status(403).send("This user don't have permission");

        const result = await this._service.PostJobAsync(body);
        if(result.success)
            return res.status(200).send({message: `Job ${body.Title} successfully created.`});

         return res.status(500).send({message: result.errors.join()});
    }

    PutAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const id = req.params.id;
        const body = res.locals.body;
        const userId = res.locals.user.id;
        
        if(id == null || body == null)
        return res.status(400).send({message: "Missing parameters"}); 

        const company = await this._service.GetCompanyById(body.CompanyId);
        if(company?.UserId !== userId)
            return res.status(403).send("This user don't have permission");
        
        const model = await this._service.GetJobById(id);
        if(model === null)
            return res.status(404).json({message: "Job not found"});
     
        const result = await this._service.PutJobAsync(id, body);
        if(result.success)
            return res.status(200).send(req.body);

        return res.status(500).send({message: result.errors.join()});
    }

    DeleteAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const id = req.params.id;
        const userId = res.locals.user.id;
        const body = req.params;
        
        const company = await this._service.GetCompanyById(body.id);
        if(company?.UserId !== userId)
            return res.status(403).send("This user don't have permission");
        
        if(id == null)
            return res.status(400).send({message: "Missing parameters."});
        
        const model = await this._service.GetJobById(id);
        if(!model)
            return res.status(404).json({message: "Job not found"});
        
        const result = await this._service.DeleteJobAsync(id);
        if(result.success)
            return res.status(200).send();
        
        return res.status(500).send({message: result.errors.join()});
    }
}