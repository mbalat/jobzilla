import { Request, Response } from 'express';
import { CompanyService } from '../../services/CompanyService';
import { UserCompanyService } from "../../services/UserCompanyService";

export default class UserCompanyController extends UserCompanyService
{
    private _service: UserCompanyService = new UserCompanyService();
    private _companyService : CompanyService = new CompanyService();

    PostAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        
        const userId = res.locals.user.id;
        const companyId = req.params.companyId
        const body = req.body;

        const company = await this._companyService.GetCompany(companyId);
        if(company === null)
            return res.status(404).send({message: "Company not found."});

        if(userId === company.UserId)
            return res.status(403).send({message: "Owner can't comment or rate their own company."});

        const checkForDuplicate = await this._service.FindDuplicateCommentAsync(userId);
        if(checkForDuplicate.count >= 1) {
            return res.status(403).send({message: "You can't comment Company more than one time."});
        }
        
        const acceptedUser = await this._service.GetAcceptedUser(userId, company.Id);
        if(acceptedUser === null)
            return res.status(403).json({message: "You don't have permission to comment or rate this Company."}); 

        const result = await this. _service.PostCommentAndRateAsync(userId, companyId, body);
        if(result.success)
            return res.status(200).json(req.body);
      
        return res.status(500).send({message: result.errors.join()});
    }
}