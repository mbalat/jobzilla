import { Request, Response } from 'express';
import { UserService } from "../../services/UserService";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { JWT_KEY } from '../../config';
import { LoginResponseRest, UserLoginResponseRest, UserLoginRest } from '../models/User/UserLogin';
import { User } from '../../services/Models/User';
import { UserRegisterRest } from '../models/User/UserRegister';
import { classToPlain } from 'class-transformer';

export default class userController extends UserService
{

    private _service: UserService = new UserService();

    FindAsync = async (req: Request, res: Response, next: any) =>
    {
        const userId = req.params.id;

        const user = await this._service.GetUserById(userId);
        if(!user)
            return res.status(404).send({message: "User not found."});

        return res.status(200).json(user);
    }

    GetAsync = async (req: Request, res: Response, next: any) =>
    {
        const userId = res.locals.user.id;

        const user = await this._service.GetUserById(userId);
        if(!user)
            return res.status(404).send({message: "User data not found."});

        return res.status(200).json(user);
    }

    LoginAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const body: UserLoginRest = res.locals.body;

        const user = await this._service.GetUserByUsername(body.UserName);
        if(!user)
            return res.status(404).send({message: "User not found."});
        
        const isPasswordValid = await bcrypt.compare(body.Password, user.Password);
        if(!isPasswordValid)
            return res.status(401).send({message: "Incorrect password."})

        const tokenOpts = {
            id: user.Id,
            username : user.UserName,
            password: user.Password,
            exp: Math.floor(Date.now() / 1000) + 24 *(60 * 60)
        }

        const userTokenData = new UserLoginResponseRest(user.id, user.UserName);

        const token = jwt.sign(tokenOpts, JWT_KEY!);
        res.header("auth-token", token);
        const response = new LoginResponseRest(userTokenData, token);
        return res.status(200).json(classToPlain(response));
        
    }

    RegisterAsync = async (req: Request, res: Response, next: any): Promise<Response> => 
    {
        const body: UserRegisterRest = res.locals.body;
        let user = new UserRegisterRest();
        user.FullName = body.FullName;
        user.UserName = body.UserName;
        user.Password = body.Password;
        user.Email = body.Email;

        
        let existingEmail = await this._service.GetByEmail(user.Email);
        if(existingEmail != null)
            return res.status(409).send({message: "User with this email already exists."});
        
        let existingUsername = await this._service.GetByUsername(user.UserName);
        if(existingUsername != null)
            return res.status(409).send({message: "Username already taken."});
        
        const result = await this._service.PostAsync(body);
        
        if(result.success)
            return res.status(200).json(req.body);

        return res.status(500).send({message: result.errors.join()});
    }

    PutAsync = async (req: Request, res: Response, next: any) => {

        const id = res.locals.user.id;
        const body = req.body;

        const result = await this._service.UpdateUserAsync(id, body);
        if(result.success)
            return res.status(200).send({message: "You successfuly updated your info."});

        return res.status(500).send({message: result.errors.join()});
    }

}

function classToPlainresponse(classToPlainresponse: any): Response<any, Record<string, any>> | PromiseLike<Response<any, Record<string, any>>> {
    throw new Error('Function not implemented.');
}
