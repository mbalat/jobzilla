import { Request, Response } from 'express'
import { CompanyService } from '../../services/CompanyService';
import { JobService } from '../../services/JobService';
import { UserJobService } from "../../services/UserJobService";

export default class userJobController extends UserJobService
{
    private _service: UserJobService = new UserJobService();
    private _jobService: JobService = new JobService();
    private _companyService: CompanyService = new CompanyService();

    GetAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const userId = res.locals.user.id;

        const applyedJobs = await this._service.GetAppliedJobsByUserId(userId);
        if(applyedJobs === null)
            return res.status(404).send({message: "There is no applyed jobs."});
        
        return res.status(200).json({items: applyedJobs});
    }

    GetApplicationsAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const userId = res.locals.user.id;
        const companyId = req.params.companyId;
        
        const company = await this._companyService.GetUserCompany(companyId, userId);
        if(company === null)
            return res.status(404).send({message: "This User don't possess company"});

        const applyedJobs = await this._service.GetAppliedJobs(company.Id);
        
        if(applyedJobs === null)
            return res.status(404).send({message: "Applications not found."});
            
        return res.status(200).send(applyedJobs);
        
    }

    FindAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const userId = res.locals.user.id;

        const savedJobs = await this._service.GetSavedJobs(userId);
        if(savedJobs === null)
            return res.status(404).send({message: "There is no saved jobs."});
        
        return res.status(200).json({items: savedJobs});
    }

    ApplyAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const userId = res.locals.user.id;
        
        const jobId = req.params.jobId;
        if(jobId == null)
            return res.status(400).send({message: "Missing parameters."});

        const result = await this._service.CreateUserJobAsync(jobId, userId);
        if(result.success)
           return res.status(200).send({status: 200,message: "You applied job."});

        return res.status(500).send({message: result.errors.join()});
    }

    SaveAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const jobId = req.params.jobId;
        const userId = res.locals.user.id;
        const body = req.body;

        const job = await this._jobService.GetJobById(jobId);

        const company = await this._companyService.GetCompany(job!.CompanyId);
        if(company === null)
            return res.status(404).send({message: "Company not found."});

        if(userId === company.UserId)
            return res.status(403).send({message: "User don't have pemission to save his own job"});

        const result = await this._service.SaveJobAsync(body, jobId, userId);
        if(result.success)
            return res.status(200).send({message: "Job is saved."});

        return res.status(500).send({message: result.errors.join()});
    }

    DeleteAsync = async (req: Request, res: Response, next: any): Promise<Response> =>
    {
        const jobId = req.params.jobId;
        const userId = res.locals.user.id;

        if(jobId === null)
            return res.status(400).send({message: "Missing parameters."});

        const result = await this._service.DeleteUserJobAsync(jobId, userId);
        if(result.success)
            return res.status(200).send({status: 200,message: "User Job deleted."});

        return res.status(500).send({message: result.errors.join()});  
    }

    ToggleAcceptedAsync = async (req: Request, res: Response, next: any) =>
    {
        const userId = res.locals.user.id;

        const userJobId = req.params.id;
        if(!userJobId)
            return res.status(400).send({message: "Missing parameters."});

        const result = await this._service.ToggleAcceptionAsync(userJobId, userId);

        if(result.success)
            return res.status(200).send({message: "User is Accepted."});

        return res.status(500).send({message: result.errors.join()});
    }

    ToggleDeniedAsync = async (req: Request, res: Response, next: any) =>
    {
        const userId = res.locals.user.id;

        const userJobId = req.params.id;
        if(!userJobId)
            return res.status(400).send({message: "Missing parameters."});

        const result = await this._service.ToggleDenialAsync(userJobId, userId);

        if(result.success)
            return res.status(200).send({message: "User is Denied."});

        return res.status(500).send({message: result.errors.join()});
    }

}