import { Request, Response } from 'express';
import jwt from "jsonwebtoken";
import { JWT_KEY } from '../../config';
import IUserToken from '../../models/IUserToken';


const auth = () =>
{
    return async (req: Request, res: Response, next: any) =>
    {
        let token = req.header("Authorization");
        if(!token)
            return res.status(401).json({message: "Access denied, Authorization missing."});
        
        if(!token.startsWith("Bearer"))
            return res.status(400).json({message: "Wrong token format."});

        token = token.slice(7, token.length).trimLeft();

        await jwt.verify(token, JWT_KEY!, (err, decoded) => {
        if(err)
            return res.status(401).json({message: "Expired token."});
        
        let loggedInUser = decoded as IUserToken;
        if(!loggedInUser)
            return res.status(401).json({message: "Access denied."});
            
            res.locals.user = loggedInUser;

            next();
        })
    }
}

export default auth;