import { Request, Response } from "express";
import { validate, ValidationError } from "class-validator";
import { plainToClass } from "class-transformer";
import _ from "lodash";

const getError = (errors: ValidationError[]): string[] =>
{
    let errStr: string[] = [];
    errors.forEach(err => {
        if(err.children && err.children.length > 0)
        {
            errStr.push(...getError(err.children));
        }
        else
        {
            errStr.push(...Object.values(err.constraints!));
        }
    });
    return errStr;
}

const validateBody = (bodyClass: any) =>
{
    return async (req: Request, res: Response, next: any) =>
    {
        if(req.body == null)
            next(res.send(400).send({message: "Model is not defined."}));

        let errors: ValidationError[] = [];
        let body: any;

        if(Array.isArray(req.body))
        {
            body = req.body.map(x => plainToClass(bodyClass, x, { excludeExtraneousValues: true}));
            for await (const res of body)
            {
                errors.push(...await validate(res, { skipMissingProperties: true }));
            }
        }
        else
        {
            body = plainToClass(bodyClass, req.body, { excludeExtraneousValues: true });
            errors.push(...await validate(body, { skipMissingProperties: true }));
        }

        if(errors.length > 0) {
            const errMapp = getError(errors);
            return next(res.status(400).send( _.uniq(errMapp)));
        }

        res.locals.body = body;

        next();
    }
}

export default validateBody;