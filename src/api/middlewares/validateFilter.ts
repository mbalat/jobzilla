import { plainToClass } from 'class-transformer';
import { Request, Response } from 'express';
import { validate } from 'class-validator';

let validateFilter = (filterClass: any) =>
{
    return async (req: Request, res: Response, next: any) =>
    {
        const filter: any = plainToClass(filterClass, req.query, { excludeExtraneousValues: true});
        let errors = await validate(filter, { skipMissingProperties: true });
        if(errors.length > 0)
            res.status(400).send(errors.map(x => Object.values(x.constraints!).join(","))).json({message: "Filter validation failed."});

        res.locals.filter = filter;

        next();
    }
}

export default validateFilter;