import { Expose } from "class-transformer";
import { IsDefined, IsNumber, IsString, Min, Max } from "class-validator";

export class CommentAndRate
{
    @Expose()
    @IsDefined()
    @IsString()
    Comment: string;

    @Expose()
    @IsDefined()
    @IsNumber()
    @Min(1)
    @Max(5)
    Rate: number;
}