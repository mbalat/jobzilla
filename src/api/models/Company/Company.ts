import { Expose } from "class-transformer";
import { IsDefined, IsNotEmpty, IsString, IsUUID, MaxLength } from "class-validator";

export class CompanyRest
{
    @Expose()
    @IsDefined()
    @IsString()
    @MaxLength(250)
    Name: string;

}