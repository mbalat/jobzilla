import { Expose, Transform } from "class-transformer";
import { IsInt, IsOptional, IsString, Min } from "class-validator";

export class FilterDTO
{
    @Expose()
    @IsOptional()
    @IsString()
    searchTerm: string;

    @Expose()
    @Transform(value => value ? Number(value) : value)
    @IsOptional()
    @IsInt()
    @Min(1,{message: "page can't be less than 1"})
    page: number;
    
    @Expose()
    @Transform(value => value ? Number(value) : value)
    @IsOptional()
    @IsInt()
    @Min(1, {message: "page can't be less than 1"})
    rpp: number;
    
    @Expose()
    @IsOptional()
    @IsString()
    sortBy: string;

    @Expose()
    @IsOptional()
    @Transform(value => value ? value.split(',') : [])
    @IsString({each:true})
    include: string[]
}

export class JobFilterDTO
{
    @Expose()
    @IsOptional()
    @IsString()
    searchTerm: string;

    @Expose()
    @Transform(value => value ? Number(value) : value)
    @IsOptional()
    @IsInt()
    @Min(1,{message: "page can't be less than 1"})
    page: number;
    
    @Expose()
    @Transform(value => value ? Number(value) : value)
    @IsOptional()
    @IsInt()
    @Min(1, {message: "page can't be less than 1"})
    rpp: number;
    
    @Expose()
    @IsOptional()
    @IsString()
    sortBy: string;

    @Expose()
    @IsOptional()
    @Transform(value => value ? value.split(',') : [])
    @IsString({each:true})
    include: string[]

    @Expose()
    @IsOptional()
    @IsString()
    companyId: string
}