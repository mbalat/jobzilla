import { Expose } from "class-transformer";
import { IsDefined, IsNumber, IsString, MaxLength, Min, IsNotEmpty, IsUUID } from "class-validator";

export class JobRest
{
    @Expose()
    @IsDefined()
    @IsString()
    @MaxLength(250)
    Title: string;

    @Expose()
    @IsDefined()
    @IsString()
    Description: string;

    @Expose()
    @IsDefined()
    @IsNumber()
    @Min(1)
    Salary: number;

    @Expose()
    @IsDefined()
    @IsString()
    @MaxLength(100)
    Location: string;

    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsUUID()
    CompanyId: string;
    
}