import { Expose } from "class-transformer";

export class UserRest
{
    @Expose({name: "id"})
    Id: string

    @Expose({name: "fullName"})
    FullName: string

    @Expose({name: "username"})
    UserName: string

    @Expose({name: "password"})
    Password: string

    @Expose({name: "email"})
    Email: string

    @Expose({name: "about"})
    About: Text
}