import { Expose } from "class-transformer";
import { IsDefined } from "class-validator";

export class UserLoginRest
{
    @Expose({name: "username"})
    @IsDefined()
    UserName: string

    @Expose({name: "password"})
    @IsDefined()
    Password: string
}

export class UserLoginResponseRest
{
    @Expose({name: "Id"})
    Id: string

    @Expose({name: "username"})
    UserName: string;

    constructor(id: string, userName: string)
    {
        this.Id = id;
        this.UserName = userName;
    }
}

export class LoginResponseRest
{
    @Expose({name: "user"})
    User: UserLoginResponseRest;

    @Expose({name: "token"})
    Token: string;

    constructor(user: UserLoginResponseRest, token: string)
    {
        this.User = user;
        this.Token = token;
    }
}