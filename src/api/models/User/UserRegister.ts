import { Expose } from "class-transformer"
import { IsDefined, Matches } from "class-validator"

export class UserRegisterRest
{
    @Expose({name: "Id"})
    Id: string

    @Expose({name: "FullName"})
    @IsDefined()
    FullName: string

    @Expose({name: "UserName"})
    @IsDefined()
    UserName: string

    @Expose({name: "Password"})
    @IsDefined()
    Password: string

    @Expose({name: "Email"})
    @IsDefined()
    @Matches(/\S+@\S+\.\S+/, {message: "Email is not in valid format."})
    Email: string

    @Expose({name: "About"})
    @IsDefined()
    About: string

}