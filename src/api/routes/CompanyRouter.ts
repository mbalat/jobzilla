import { Router } from "express";
import CompanyController from "../controllers/CompanyController";
import auth from "../middlewares/auth";
import validateBody from "../middlewares/validadteBody";
import validateFilter from "../middlewares/validateFilter";
import { CompanyRest } from "../models/Company/Company";
import { FilterDTO } from "../models/Filter";

const companyController = new CompanyController();

const companyRouter = Router();
companyRouter.use(auth());

companyRouter.get("/", validateFilter(FilterDTO), companyController.FindAsync);
companyRouter.post("/", validateBody(CompanyRest) ,companyController.PostAsync);

companyRouter.get("/my-companies", companyController.GetMyAsync);
companyRouter.get("/:id", companyController.GetAsync);
companyRouter.get("/:id/rate", companyController.GetRateAsync);
companyRouter.get("/:id/comments", companyController.GetCommentsAsync);

companyRouter.put("/:id", validateBody(CompanyRest), companyController.PutAsync);

companyRouter.delete("/:id", companyController.DeleteAsync);

export default companyRouter;