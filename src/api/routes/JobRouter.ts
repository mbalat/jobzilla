import { Router } from 'express'
import JobController from '../controllers/JobController';
import auth from '../middlewares/auth';
import validateBody from '../middlewares/validadteBody';
import validateFilter from '../middlewares/validateFilter';
import { JobFilterDTO } from '../models/Filter';
import { JobRest } from '../models/Job/JobRest';

const jobController = new JobController();

const jobRouter = Router();
jobRouter.use(auth());

jobRouter.get("/", validateFilter(JobFilterDTO), jobController.FindAsync);
jobRouter.get("/:id", jobController.GetAsync);
jobRouter.post("/", validateBody(JobRest), jobController.PostAsync);
jobRouter.put("/:id", jobController.PutAsync);
jobRouter.delete("/:id", jobController.DeleteAsync);

export default jobRouter;