import { Router } from "express";
import UserCompanyController from "../controllers/UserCompanyController";
import auth from "../middlewares/auth";
import validateBody from "../middlewares/validadteBody";
import { CommentAndRate } from "../models/CommentAndRate";

const userCompanyController = new UserCompanyController();

const userCompanyRouter = Router();
userCompanyRouter.use(auth());

userCompanyRouter.post("/:companyId", validateBody(CommentAndRate), userCompanyController.PostAsync);

export default userCompanyRouter;