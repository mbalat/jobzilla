import { Router } from "express";
import UserJobController from "../controllers/UserJobController";
import auth from "../middlewares/auth";

const userJobController = new UserJobController;

const userJobRouter = Router();
userJobRouter.use(auth());

userJobRouter.get("/", userJobController.FindAsync); 
userJobRouter.get("/applyed-jobs", userJobController.GetAsync);
userJobRouter.get("/:companyId", userJobController.GetApplicationsAsync);

userJobRouter.put("/:jobId/apply", userJobController.ApplyAsync);
userJobRouter.put("/:id/accept", userJobController.ToggleAcceptedAsync);
userJobRouter.put("/:id/deny", userJobController.ToggleDeniedAsync);

userJobRouter.post("/:jobId", userJobController.SaveAsync);

userJobRouter.delete("/:jobId", userJobController.DeleteAsync);


export default userJobRouter; 