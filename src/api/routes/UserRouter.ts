import { Router } from "express";
import UserController from "../controllers/UserController";
import validateBody from "../middlewares/validadteBody";
import { UserLoginRest } from "../models/User/UserLogin";
import { UserRegisterRest } from "../models/User/UserRegister";
import auth from '../middlewares/auth';

const userController = new UserController();

const userRouter = Router();

userRouter.get("/profile", auth(), userController.GetAsync);
userRouter.get("/profile/:id", auth(), userController.FindAsync);
userRouter.post("/login", validateBody(UserLoginRest), userController.LoginAsync);
userRouter.post("/register", validateBody(UserRegisterRest), userController.RegisterAsync);
userRouter.put("/edit-profile", auth(), userController.PutAsync);


export default userRouter;