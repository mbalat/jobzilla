import { Router } from 'express'
import companyRouter from './CompanyRouter';
import jobRouter from './JobRouter'
import userCompanyRouter from './UserCompanyRouter';
import userJobRouter from './UserJobRouter';
import userRouter from './UserRouter';
const router = Router();
router.use("/jobs", jobRouter);
router.use("/companies", companyRouter);
router.use("/user-companies", userCompanyRouter);
router.use("/users", userRouter);
router.use("/user-jobs", userJobRouter);

export default router;