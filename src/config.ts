import dotenv from "dotenv";
import path from "path";

dotenv.config({
    path: path.resolve(`config/${process.env.NODE_ENV}.env`)
}); 

const NODE_ENV = process.env.NODE_ENV || 'development';

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 8000;

const DB_HOST = process.env.DB_HOST || 'localhost';
const DB_USERNAME = process.env.DB_USERNAME || 'root';
const DB_PASSWORD = process.env.DB_PASSWORD || 'reroot';
const DB_DATABASE = process.env.DB_DATABASE || 'JobZilla';

const JWT_KEY = process.env.JWT_KEY;

export {
    NODE_ENV,
    HOST,
    PORT,
    DB_HOST,
    DB_USERNAME,
    DB_PASSWORD,
    DB_DATABASE,
    JWT_KEY
}