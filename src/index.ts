import express from 'express'
import router from './api/routes/';
import {HOST, PORT} from './config';
import cors from 'cors'

const baseUrl = '/jobzilla';

const app: any = express();

var corsOptions = {
  origin: 'http://localhost:3000'
};

app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(baseUrl, router);

(async () => {
  //await sequelize.sync({force: true});
  app.listen(Number(PORT), HOST, () => {
    console.log(`http://${HOST}:${PORT}`)
});
})();