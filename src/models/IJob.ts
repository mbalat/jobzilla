export default interface IJob
{
    Id?: string
    Title: string
    Description: string
    Salary: number
    Location: string
    DateCreated?: number
    DateUpdated?: number
    CompanyId: string
}