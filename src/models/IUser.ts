export default interface IUser
{
    Id: string;
    FullName: string;
    UserName: string;
    Password: string;
    Email: string;
}

export default interface IUserUpdate
{
    Email: string;
    About: string;
}