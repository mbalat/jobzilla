import { DataTypes, Model } from "sequelize";

export default interface IUserCompany
{
    Id: string
    CompanyId: string
    UserId: string
    Rate: number
    DateCreated: number
    Comment: Text
}