export default interface IUserJob
{
    Id: string,
    JobId: string,
    UserId: string,
    IsApplyed: boolean,
    IsAccepted: boolean | null
}