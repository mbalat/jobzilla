export default interface IUserToken
{
    Id: string
    UserName: string
    Password: string
    Email: string
}