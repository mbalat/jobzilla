import { Op } from "sequelize";
import { ActionResult } from "../ActionResult";
import ICompany from "../models/ICompany";
import { Company } from "./Models/Company";
import { UserCompany } from "./Models/UserCompany";
import { v4 } from "uuid";
import { User } from "./Models/User";
import { Sequelize } from "sequelize";

export class CompanyService 
{
    GetByUser = (userId: string): Promise<Company[]> =>
    {
        return Company.findAll({ where: {UserId: userId}});
    }

    GetCompanyCommentsAsync = async (companyId: string): Promise<UserCompany[]> =>
    {
        return UserCompany.findAll({
            where: {CompanyId: companyId},
            attributes:["Comment","Rate","DateCreated"], 
            order: [["DateCreated", 'DESC']],
            include: [{
                attributes: ["UserName"],
                model: User,
            }]
        });
        //Alter table UserCompany ADD CONSTRAINT UC_UserCompany UNIQUE (CompanyId, UserId);
    }

    GetByName = (name: string): Promise<Company | null> =>
    {
        return Company.findOne({where: {Name: name}});
    }

    FindCompanyAsync = async (filter: any): Promise<{rows: Company[],count: number}> =>
    {
        const page = filter.page ? Number(filter.page) : 1;
        const rpp = filter.rpp ? Number(filter.rpp) : 10;

        let options = {
            limit: rpp,
            offset: rpp * (page - 1)
        };

        if(filter.searchTerm)
            Object.assign(options, {where: {Name:{[Op.like]: `%${filter.searchTerm}%`}}});
        
        if(filter.sortBy)
            Object.assign(options, {order: ['Name']});

        if(filter.include)
            Object.assign(options, {include:[{
                model: User,
                attributes: ["UserName"],
            }]});

        const companies = await Company.findAndCountAll(options);

        return({rows: companies.rows, count: companies.count});
    }

    GetCompany = (id: string): Promise<Company | null> =>
    {
        return Company.findByPk(id);
    }

    GetUserCompany = (companyId: string, userId: string): Promise<Company | null> =>
    {
        return Company.findOne({where: {Id: companyId, UserId: userId}});
    }

    GetAvgCompanyRateAsync = async (companyId: string): Promise<number | null> =>
    {
        const rate = await UserCompany.findOne({
            where: {CompanyId: companyId},
            attributes: [
                [Sequelize.fn('ROUND', Sequelize.fn('avg', Sequelize.col('Rate')),1), 'Rate']
            ],
        });
        
        return rate!.Rate;
    }

    PostCompanyAsync = async (userId: string, company: ICompany): Promise<ActionResult> =>
    {
        const result = new ActionResult;

        company.Id = v4();
        company.UserId = userId;

        const createdResult = await Company.create(company);
        
        if(!createdResult)
            result.AddError("Company is not added")

        return result;
    }

    PutCompanyAsync = async (id: string, company: ICompany): Promise<ActionResult> =>
    {
        const result = new ActionResult();

        const updatedResult = await Company.update(company, {where: {Id: id}});
        if(updatedResult[0] != 1) {
            result.AddError("Not updated");
        }

        return result;
    }

    DeleteCompanyAsync = async (id: string): Promise<ActionResult> =>
    {
        const result = new ActionResult();

        const deletedResult = await Company.destroy({where: {Id: id}});
        if(deletedResult != 1)
            result.AddError("Not deleted");

        return result;
    }

}