import { Op } from "sequelize";
import { v4 } from "uuid";
import { ActionResult } from "../ActionResult"
import ICompany from "../models/ICompany";
import IJob from "../models/IJob";
import { Company } from "./Models/Company";
import { Job } from "./Models/Job";

export class JobService {
    
    FindJobAsync = async (filter: any): Promise<{rows: Job[], count: number}> =>
    {
        const page = filter.page ? Number(filter.page) : 1;
        const rpp = filter.rpp ? Number(filter.rpp) : 10;

        let options = {
            limit: rpp,
            offset: rpp * (page - 1)
        };

        if(filter.companyId)
            Object.assign(options, {where: {CompanyId: filter.companyId}});

        if(filter.searchTerm)
            Object.assign(options, {where: {Title:{[Op.like]: `%${filter.searchTerm}%`}}});
            
        if(filter.sortBy === "title")
            Object.assign(options, {order: ['Title']});
        
        const jobs = await Job.findAndCountAll(options);

        return ({rows: jobs.rows, count: jobs.count});
    }

    
    PostJobAsync = async (job: IJob): Promise<ActionResult> =>
    {
        const result = new ActionResult();
        
        job.Id = v4();
        job.DateCreated = new Date().getTime();
        job.CompanyId = job.CompanyId;
        
        const createdResult = await Job.create(job);
        if(!createdResult)
            result.AddError("Job is not added");
        
        return result;
    }
    
    PutJobAsync = async (id: string, job: IJob): Promise<ActionResult> =>
    {
        const result = new ActionResult();
        
        const updatedResult = await Job.update(job, {where: {Id: id}});
        if(updatedResult[0] != 1) 
            result.AddError("Not updated");
        
        return result;
    }
    
    DeleteJobAsync = async (id: string): Promise<ActionResult> => {
        const result = new ActionResult();
        
        const deletedResult = await Job.destroy({where: {Id: id}});
        if(deletedResult != 1)
            result.AddError("Not deleted");
        
        return result;  
    }

    GetCompanyById = (companyId: string): Promise<Company | null> =>
    {
        return Company.findByPk(companyId);  
    }
    
    GetJobById = (id: string): Promise<Job | null> =>
    {
        return Job.findByPk(id);
    }
    
    FindCompany = (userId: string): Promise<Company | null> => 
    {  
        return Company.findOne({where: {UserId: userId}});
    }

    FindCompanyJobs = (companyId: string): Promise<Job[]> =>
    {
        return Job.findAll({where: {CompanyId: companyId}})
    }
}