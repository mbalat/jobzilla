import { BelongsTo, Column, ForeignKey, Model, Table } from "sequelize-typescript";
import { sequelize } from "../database";
import { User } from "./User";

@Table
export class Company extends Model
{
    @Column({primaryKey: true})
    Id: string;

    @Column
    Name: string;

    @ForeignKey(() => User)
    @Column
    UserId: string;

    @BelongsTo(() => User)
    User: User;

    // @BelongsToMany(() => User, () => UserCompany)
    // Users: User[];
}

sequelize.addModels([Company]);