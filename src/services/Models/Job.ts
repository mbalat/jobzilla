import { BelongsTo, Column, DataType, Default, ForeignKey, Model, Table } from "sequelize-typescript";
import { sequelize } from "../database";
import { Company } from "./Company";

@Table
export class Job extends Model
{
    @Column({primaryKey: true})
    Id: string;

    @ForeignKey(() => Company)
    @Column
    CompanyId: string;

    @Column
    Title: string;

    @Column(DataType.TEXT)
    Description: string;

    @Column
    Salary: number;
    
    @Column
    Location: string;

    @Column
    DateCreated: number;

    @Default(new Date().getTime())
    @Column
    DateUpdated: number;

    @BelongsTo(() => Company)
    Company: Company
}

sequelize.addModels([Job]);