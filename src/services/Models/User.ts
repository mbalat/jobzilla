import { Table, Model, Column } from "sequelize-typescript";
import { sequelize } from "../database";

@Table
export class User extends Model
{    
    @Column({primaryKey: true})
    Id: string;

    @Column
    FullName: string;

    @Column
    UserName: string;
    
    @Column
    Password: string;

    @Column
    Email: string;

    @Column
    About: string;

}

sequelize.addModels([User]);