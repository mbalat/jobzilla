import { Table, Model, Column, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Company } from "./Company";
import { User } from "./User";
import { sequelize } from "../database";

 @Table
 export class UserCompany extends Model
 {
    @Column({primaryKey: true})
    Id: string;

    @ForeignKey(() => Company)
    @Column
    CompanyId: string;

    @ForeignKey(() => User)
    @Column
    UserId: string;

    @Column
    DateCreated: number;

    @Column
    Rate: number;

    @Column
    Comment: string;

    @BelongsTo(() => User)
    User: User
 }

sequelize.addModels([UserCompany]);