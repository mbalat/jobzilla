import { Column, Table, Model, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Job } from "./Job";
import { User } from "./User";
import { sequelize } from "../database";

@Table
export class UserJob extends Model
{
    @Column({primaryKey: true})
    Id: string;

    @ForeignKey(() => User)
    @Column
    UserId: string

    @ForeignKey(() => Job)
    @Column
    JobId: string;

    @Column
    IsApplyed: boolean;

    @Column
    IsAccepted: boolean;

    @BelongsTo(() => Job)
    Job: Job;

    @BelongsTo(() => User)
    User: User;
}

sequelize.addModels([UserJob]);