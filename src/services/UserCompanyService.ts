import { v4 } from "uuid";
import { ActionResult } from "../ActionResult";
import IUserCompany from "../models/IUserCompany";
import { Job } from "./Models/Job";
import { UserCompany } from "./Models/UserCompany";
import { UserJob } from "./Models/UserJob";

export class UserCompanyService
{
    PostCommentAndRateAsync = async (userId: string, companyId: string, userCompany: IUserCompany): Promise<ActionResult> =>
    {
        const result = new ActionResult();

            userCompany.Id = v4();
            userCompany.CompanyId = companyId;
            userCompany.UserId = userId;
            userCompany.DateCreated = new Date().getTime();
    
            if(userCompany.Comment === null || userCompany.Rate === null)
                result.AddError("Comment or Rate is missing.");
    
            const createdResult = await UserCompany.create(userCompany);
            if(createdResult === null) {
                result.AddError("User Company is not added");
            } 
            else {
                result.success;
            }
              
        return result;
    }

    GetAcceptedUser = (userId: string, companyId: string): Promise<UserJob | null> =>
    {
        return UserJob.findOne({
            where: {UserId: userId, IsAccepted: true},
            include:[{
                model:Job,
                where: {CompanyId: companyId}
            }]
        });
    }

    FindDuplicateCommentAsync = async (userId: string): Promise<{rows: UserCompany[], count: number}> =>
    {
        const userCompany = await UserCompany.findAndCountAll({where: {UserId: userId}});
        return ({rows: userCompany.rows, count: userCompany.count});
    }
}