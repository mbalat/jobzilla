import { v4 } from "uuid";
import { ActionResult } from "../ActionResult";
import IUserJob from "../models/IUserJob";
import { Company } from "./Models/Company";
import { Job } from "./Models/Job";
import { User } from "./Models/User";
import { UserJob } from "./Models/UserJob";

export class UserJobService
{

    GetAppliedJobsByUserId = (userId: string): Promise<UserJob[]> =>
    {
        return UserJob.findAll({
            where: {UserId: userId, IsApplyed: true},
            include: [{
                model: Job,
            }]
        });
    }

    GetSavedJobs = (userId: string): Promise<UserJob[]> =>
    {
        return UserJob.findAll({
            where: {UserId: userId}, 
            include: [{
                model: Job, 
                include: [{
                    attributes: ["Name"],
                    model: Company
                }]
            }]
        });
    }

    
    SaveJobAsync = async (userJob: IUserJob, jobId: string, userId: string): Promise<ActionResult> =>
    {
        const result = new ActionResult(); 
            userJob.Id = v4();
            userJob.JobId = jobId;
            userJob.UserId = userId;
            userJob.IsApplyed = false;

            const createdResult = await UserJob.create(userJob);
            if(!createdResult) {
                result.AddError("Job is not saved.");
            }
            else {
                result.success;
            }
         return result;
    }
    
    DeleteUserJobAsync = async (jobId: string, userId: string): Promise<ActionResult> =>
    {
        const result = new ActionResult;
        
        const deleteResult = await UserJob.destroy({where: {JobId: jobId, UserId: userId}});
        if(deleteResult !== 1)
        result.AddError("Such item doesn't exist.");
        
        return result;
    }

    CreateUserJobAsync = async (jobId: string, userId: string): Promise<ActionResult> =>
    {
        const result = new ActionResult();
        
        const company = await this.GetCompanyOwner(userId);

        if(company?.UserId === userId)
        {
            result.AddError("You can't save your own job.");
        } 
        else {
            let userJob = await this.GetUserJob(jobId, userId);   
            if(userJob !== null) {
                userJob.IsApplyed = true;
                const updateResult = await userJob.save();        
    
                if(!updateResult)
                    result.AddError("UserJob is not updated.");
                    
            } else {  
                const newUserJob = {
                    Id : v4(),
                    JobId: jobId,
                    UserId: userId,
                    IsApplyed: true
                };
    
                const newResult = await UserJob.create(newUserJob);
                if(!newResult)
                    result.AddError("UserJob is not created.");
            }
        }

        return result;
    }

    GetByCompany = (companyId: string): Promise<UserJob[]> =>
    {
        return UserJob.findAll({
            where: {IsApplyed: true},
            include:[
                {
                model: Job,
                where: {CompanyId: companyId}
                },
                {
                    model: User
                }
            ]
        })
    }

    GetAppliedJobs = (companyId: string): Promise<UserJob[]> =>
    {
        return UserJob.findAll({
            where: {IsApplyed: true},
            include:[{
                model: Job,
                where: {CompanyId: companyId}
            },
            {
                model: User
            }
        ]
        });
    }

    ToggleAcceptionAsync = async (userJobId: string, userId: string): Promise<ActionResult> =>
    {
        const result = new ActionResult();
        const userJob = await UserJob.findByPk(userJobId);

        const uj = await UserJob.findOne({
            where: {Id: userJobId, IsApplyed: true},
            include:[{
                model: Job,
                where: {Id: userJob?.JobId}
            }]
        });

        if(uj === null)
            result.AddError("Can't find User Job.");

        const company = await Company.findByPk(uj?.Job.CompanyId);
        if(company?.UserId === userId) {
            userJob!.IsAccepted = true;
            const updatedResult = await userJob!.save();
            result.success;
        } else {
            result.AddError("You don't have permission");
        }
        
        return result;
        
    }

    ToggleDenialAsync = async (userJobId: string, userId: string): Promise<ActionResult> =>
    {
        const result = new ActionResult();
        const userJob = await UserJob.findByPk(userJobId);

        const uj = await UserJob.findOne({
            where: {Id: userJobId, IsApplyed: true},
            include:[{
                model: Job,
                where: {Id: userJob?.JobId}
            }]
        });

        if(uj === null)
            result.AddError("Can't find User Job.");

        const company = await Company.findByPk(uj?.Job.CompanyId);
        if(company?.UserId === userId) {
            userJob!.IsAccepted = false;
            const updatedResult = await userJob!.save();
            result.success;
        } else {
            result.AddError("You don't have permission");
        }
        
        return result;
        
    }

    private GetCompanyOwner = (userId: string): Promise<Company | null> =>
    {
        return Company.findOne({where: {UserId: userId}});
    }

    private GetUserJob = (jobId: string, userId?: string): Promise<UserJob | null> =>
    {
        return UserJob.findOne({where: {JobId: jobId, UserId: userId}});
    }
}