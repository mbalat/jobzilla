import { v4 } from 'uuid';
import { ActionResult } from "../ActionResult";
import IUser from "../models/IUser";
import IUserUpdate from "../models/IUser";
import { User } from "./Models/User";
import bcrypt from 'bcrypt'

export class UserService {

    GetUserById = (userId: string): Promise<User | null> =>
    {
        return User.findByPk(userId);
    }
    
    PostAsync = async (user: IUser): Promise<ActionResult> =>
    {
        const result = new ActionResult();
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(user.Password, salt);
        
        user.Id = v4();
        user.Password = hashPassword;
        
        const createResult = await User.create(user);
        if(!createResult)
            result.AddError("Register Failed");
        
        return result;
    }
    
    GetUserByUsername = (username: string):  Promise<User | null> =>
    {
        return User.findOne({where: {UserName: username}});
    }
    
    GetByEmail = (email: string): Promise<User | null>  =>
    {
        return User.findOne({where: {Email: email}});   
    }

    GetByUsername = (username: string): Promise<User | null> =>
    {
        return User.findOne({where: {UserName: username}});
    }

    UpdateUserAsync = async (userId: string, user: IUserUpdate): Promise<ActionResult> =>
    {
        const result = new ActionResult();

        const updatedResult = await User.update(user, {where: {Id: userId}});
        if(updatedResult[0] != 1){
            result.AddError("Not updated.");
        } else {
            result.success;
        }

        return result;
    }
}