import { Sequelize } from 'sequelize-typescript'
import { DB_DATABASE, DB_PASSWORD, DB_USERNAME } from "../config";

export const sequelize = new Sequelize(
    {
        dialect: 'mysql',
        database: DB_DATABASE,
        username: DB_USERNAME,
        password: DB_PASSWORD,
        models: [__dirname + '/src/services/Models/'],
        define: {
            timestamps: false,
            freezeTableName: true
        }
    }
);